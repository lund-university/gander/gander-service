@ECHO OFF

IF "%1"=="" GOTO portsNotIncluded

set ganderPort=%1
set /a guthubConnectPort=%1+1
set /a compilerPort=%1+2
set /a diffPort=%1+3
set /a annotationPort=%1+4
set /a storagePort=%1+5
set /a gerritConnectPort=%1+8
GOTO startingServices

:portsNotIncluded
set ganderPort       =   8001
set guthubConnectPort         =   8002
set compilerPort    =   8003
set diffPort        =   8004
set annotationPort  =   8005
set storagePort     =   8006
set replayPort      =   8007
set fixationPort    =   8008
set gerritConnectPort         =   8009
ECHO Ports not defined, using default port numbers ..

:startingServices

ECHO Starting Gander Service on port %ganderPort%..
start cmd.exe /k "TITLE Gander Service & gradle run --args='%ganderPort%'"

ECHO Gander service started!

ECHO Starting Gander Client
cd ../gander-client
start cmd.exe /k "TITLE Gander Client & webdev serve"
ECHO Gander client started!

ECHO Starting githubConnect Service on port %guthubConnectPort%..
cd ../github-connect-service
start cmd.exe /k "TITLE githubConnect Service & gradle run --args='%guthubConnectPort%'"
ECHO githubConnect service started!

@REM ECHO Starting Compiler Service on port %compilerPort%..
@REM cd ../compiler-service
@REM start cmd.exe /k "TITLE Compiler Service & gradle run --args='%compilerPort%'"
@REM ECHO Compiler service started!

ECHO Starting Parser Service on port %compilerPort%..
cd ../parser-service
start cmd.exe /k "TITLE Parser Service & gradle run --args='%compilerPort%'"
ECHO Parser service started!

ECHO Starting Diff Service on port %diffPort%..
cd ../diff-service
start cmd.exe /k "TITLE Diff Service & gradle run --args='%diffPort%'"
ECHO Diff service started!

ECHO Starting Annotation Service on port %annotationPort%..
cd ../annotation-service
start cmd.exe /k "TITLE Annotation Service & gradle run --args='%annotationPort%'"
ECHO Annotation service started!

ECHO Starting Storage Service on port %storagePort%..
cd ../storage-service
start cmd.exe /k "TITLE Storage Service & gradle run --args='%storagePort%'"
ECHO Storage service started!

ECHO Starting Replay Service on port %replayPort%..
cd ../replay-service
start cmd.exe /k "TITLE Replay Service & gradle run --args='%replayPort%'"
ECHO Replay service started!

ECHO Starting Fixation Service on port %fixationPort%..
cd ../fixation-service/App
start cmd.exe /k "TITLE Fixation Service & Python main.py"
ECHO Fixation Service started!

ECHO Starting Gerrit Connect Service on port %gerritConnectPort%..
cd ../gerrit-connect-service
start cmd.exe /k "TITLE Gerrit Connect Service & gradle run --args='%gerritConnectPort%'"
ECHO Gerrit Connect service started!

cd ../../gander-service
PAUSE