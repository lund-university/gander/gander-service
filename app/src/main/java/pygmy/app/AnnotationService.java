package pygmy.app;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.logging.Logger;
import spark.Request;
import spark.Response;

class AnnotationService {

  private static final String ANNOTATION_NAME = PathHandler.getAnnotationPath();

  private static final String PING_PATH = "/ping";
  private static final String COMMENT_PATH = "/comments";
  private Logger logger;

  private static final String CLASS_NAME = AnnotationService.class.getName();

  public AnnotationService(Logger log) {
    this.logger = log;
    logger.info("AnnotationService started");
  }

  public String ping(Request req, Response res) {
    logger.entering(CLASS_NAME, "ping");
    String urlString = String.format("%s%s%n", ANNOTATION_NAME, PING_PATH);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "ping");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String errPing =
            String.format("%d: Error while pinging the annotation service.", c.getResponseCode());
        logger.severe(errPing);
        return errPing;
      }
    } catch (IOException e) {
      logger.info(e.getMessage());
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  public String getComments(Request req, Response res) {
    logger.entering(CLASS_NAME, "getComments");
    String urlString =
        String.format(
            "%s%s/%s/%s", ANNOTATION_NAME, COMMENT_PATH, req.params(":mode"), req.params(":number"));
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");
      logger.fine("Connection opened");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getComments");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String errPing =
            String.format(
                "%d: Error while retrieving comments from the annotation service.",
                c.getResponseCode());
        logger.severe(errPing);
        return errPing;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  public String postComment(Request req, Response res) {
    logger.entering(CLASS_NAME, "postComment");
    String urlString =
        String.format(
            "%s%s/%s", ANNOTATION_NAME, COMMENT_PATH, req.params(":number"));
    String b = req.body();
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {
        byte[] input = b.getBytes("utf-8");
        os.write(input, 0, input.length);
      }
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "postComment");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String errPing =
            String.format(
                "%d: Error while putting comment in the annotation service.", c.getResponseCode());
        logger.severe(errPing);
        return errPing;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }
}
