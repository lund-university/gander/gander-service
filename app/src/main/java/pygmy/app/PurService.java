package pygmy.app;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;
import spark.Request;
import spark.Response;
//import pygmy.PathHandler;

/** Class for communicating with the Pur Service. */
class PurService {


  private static final String PUR_NAME = PathHandler.getGithubConnectorPath();

  private static final String PUR_PATH = "/pur";
  private static final String CLASS_NAME = PurService.class.getName();
  private Logger logger;

  public PurService(Logger log) {
    this.logger = log;
    logger.info("PurService started");
  }

  /**
   * Adds a file to the Pur Service. 
   * <p> Currently not used, since we are not creating any new files. 
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the Pur Service response.
   * @throws IOException
   */
  public String addPur(Request req, Response res) {
    String urlString = String.format("%s%s", PUR_NAME, PUR_PATH);
    String b = req.body();
    logger.entering(CLASS_NAME, "addPur", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {
        byte[] input = b.getBytes("utf-8");
        os.write(input, 0, input.length);
      }
      if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
        logger.exiting(CLASS_NAME, "addPur");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err = String.format("%d: File could not be added", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

 /**
   * Modifies a file in the Pur Service, and updates the file content. 
   * 
   * @param req The HTTP Request. Should contain the param ":name".
   * @param res The HTTP Response.
   * @return A JSON String containing the Pur Service response.
   * @throws IOException
   */
  public String putPurFile(Request req, Response res) {
    String urlString =
        String.format("%s%s/%s", PUR_NAME, PUR_PATH, req.params(":name"));
    String b = req.body();
    logger.entering(CLASS_NAME, "putPurFile", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "PUT");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {
        byte[] input = b.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "putPurFile");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err = String.format("%d: File could not be modified", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

 /**
   * Retrieves all of the files stored in the Pur Service.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the Pur Files from Pur Service.
   * @throws IOException
   */
  public String getPurs(Request req, Response res) {
    String urlString = String.format("%s%s%n", PUR_NAME, PUR_PATH);
    logger.entering(CLASS_NAME, "getPurs", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPurs");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err = String.format("%d: Error while retrieving the files.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

 /**
   * Retrieves the specified Pur File from the Pur Service. 
   * 
   * @param req The HTTP Request. Should contain the params ":name" and ":sha". It could also contain ":nestedFile", that is used
   * to show the hierarchary since we cannot add the '/'-character to a request.
   * @param res The HTTP Response.
   * @return A JSON String containing the Pur File.
   * @throws IOException
   */
  public String getPurFile(Request req, Response res) throws UnsupportedEncodingException {
    String sha = req.queryParams("sha");
    String name = URLEncoder.encode(req.params(":name"), StandardCharsets.UTF_8.toString());
    if (req.params(":nestedFile") != null) {
      name += "/" + req.params(":nestedFile");
    }
    String urlString;
    if (sha != null) {
      urlString = String.format("%s%s/%s?sha=%s", PUR_NAME, PUR_PATH, name, sha);

    } else {
      urlString = String.format("%s%s/%s", PUR_NAME, PUR_PATH, name);
    }

    logger.entering(CLASS_NAME, "getPurFile", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPurFile");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the file %s", c.getResponseCode(), req.params(":name"));
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }
}
