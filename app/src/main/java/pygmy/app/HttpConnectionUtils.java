package pygmy.app;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/** A class for establishing the HTTP Connections */
class HTTPConnectionUtils {

  /**
   * Establishes a connection with content-type json.
   * 
   * @param urlString The string containing the URL
   * @param method What HTTP method, for example "GET"
   * @return The established HTTPURLConnection
   * @throws IOException
   */
  public static HttpURLConnection setConnection(String urlString, String method)
      throws IOException {
    HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, method);
    c.setRequestProperty("Content-Type", "application/json");
    c.setRequestProperty("Accept", "application/json");
    return c;
  }

 /**
   * Establishes a connection without any headers.
   * 
   * @param urlString The string containing the URL
   * @param method What HTTP method, for example "GET"
   * @return The established HTTPURLConnection
   * @throws IOException
   */
  public static HttpURLConnection setConnectionNoHeaders(String urlString, String method)
      throws IOException {
    URL url = new URL(urlString);
    HttpURLConnection c = (HttpURLConnection) url.openConnection();
    c.setRequestMethod(method);
    return c;
  }
}
