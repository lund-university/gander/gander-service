package pygmy.app;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.logging.Logger;
import spark.Request;
import spark.Response;
//import pygmy.PathHandler;

/** Class for communicating with the Diff Service. */
class DiffService {
  
  private static final String DIFF_SERVICE_PATH = PathHandler.getDiffPath();
  
  private static final String DIFF_PING = "/ping";
  private static final String DIFF_COMPARE_PATH = "/compare";
  private static final String COMPARE_FILES = "/compareFiles";
  private static final String DIFF_PATH = "/diff";
  private static final String PULL_PATH = "/pulls";
  private static final String FILE_PATH = "/files";
  private static final String CHANGES_PATH = "/changes";

  private static final String CLASS_NAME = DiffService.class.getName();

  private Logger logger;

  public DiffService(Logger log) {
    this.logger = log;
    logger.info("DiffService started");
  }

  /**
   * Method for testing the communication with the Diff Service.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the response.
   * @throws IOException
   */
  public String ping(Request req, Response res) {
    logger.entering(CLASS_NAME, "ping");
    String urlString = String.format("%s%s", DIFF_SERVICE_PATH, DIFF_PING);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "ping");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String errPing =
            String.format("%d: Error while pinging the diff service.", c.getResponseCode());
        logger.severe(errPing);
        return errPing;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path to diff service: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves the diff patches from the specified pull request given in the HTTPRequest params.
   * @param req The HTTP Request. Should have a param ":pull_number".
   * @param res The HTTP Response.
   * @return A JSON String containing the diff patches.
   * @throws IOException
   */
  public String getPullRequestPatch(Request req, Response res) {
    String pullNumber = req.params(":pull_number");

    String urlString =
        String.format("%s%s%s/%s",DIFF_SERVICE_PATH, DIFF_PATH, PULL_PATH, pullNumber);
    logger.entering(CLASS_NAME, "getPullRequestPatch", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPullRequestPatch");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the pull request patch.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

   /**
   * Retrieves the diff patches from the specified pull request given in the HTTPRequest params.
   * @param req The HTTP Request. Should have a params ":owner", ":repo", and ":pull_number".
   * @param res The HTTP Response.
   * @return A JSON String containing the diff patches.
   * @throws IOException
   */
  public String getPullRequestPatchFromUrl(Request req, Response res) {
    String owner = req.params(":owner");
    String repo = req.params(":repo");
    String pullNumber = req.params(":pull_number");

    String urlString =
        String.format("%s%s%s/%s/%s/%s",DIFF_SERVICE_PATH, DIFF_PATH, PULL_PATH, owner, repo, pullNumber);
    logger.entering(CLASS_NAME, "getPullRequestPatch", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPullRequestPatch");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the pull request patch.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Compares the two files that is sent to receive a diff patch.
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the diff patch.
   * @throws IOException
   */
  public String compareFiles(Request req, Response res) {
    String b = req.body();
    String urlString = String.format("%s%s", DIFF_SERVICE_PATH, COMPARE_FILES);
    logger.entering(CLASS_NAME, "compareFiles", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {
        byte[] input = b.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "compareFiles");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err = String.format("%d: File could not be modified", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves the files that is included in a pull request.
   * @param req The HTTP Request. Should have a param ":pull_number".
   * @param res The HTTP Response.
   * @return A JSON String containing pull request files.
   * @throws IOException
   */
  public String getPullRequestFiles(Request req, Response res) {
    String pullNumber = req.params(":pull_number");

    String urlString =
        String.format(
            "%s%s%s/%s%s", DIFF_SERVICE_PATH, DIFF_PATH, PULL_PATH, pullNumber, FILE_PATH);
    logger.entering(CLASS_NAME, "getPullRequestFiles", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPullRequestFiles");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the pull request files.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

    /**
   * Retrieves the files that is included in a pull request.
   * @param req The HTTP Request. Should have a params ":owner", ":repo", and ":pull_number".
   * @param res The HTTP Response.
   * @return A JSON String containing pull request files.
   * @throws IOException
   */
  public String getPullRequestFilesFromUrl(Request req, Response res) {
    String owner = req.params(":owner");
    String repo = req.params(":repo");
    String pullNumber = req.params(":pull_number");

    String urlString =
        String.format(
            "%s%s%s/%s/%s/%s/%s", DIFF_SERVICE_PATH, DIFF_PATH, PULL_PATH, owner, repo, pullNumber, FILE_PATH);
    logger.entering(CLASS_NAME, "getPullRequestFiles", urlString);

    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPullRequestFiles");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the pull request files.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }


  /**
   * Compares two commits to see what changes have been done.
   * 
   * @param req The HTTP Request. Should have a param ":commits".
   * @param res The HTTP Response.
   * @return A JSON String containing the comparison.
   * @throws IOException
   */
  public String getComparisonOfCommits(Request req, Response res) {
    String commits = req.params(":commits");
    String urlString =
        String.format("%s%s%s/%s", DIFF_SERVICE_PATH, DIFF_PATH, DIFF_COMPARE_PATH, commits);
    logger.entering(CLASS_NAME, "getComparisonOfCommits", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getComparisonOfCommits");
        return JSONUtils.processArrayResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while getting the commit comparison: %s",
                c.getResponseCode(), c.getResponseMessage());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path to diff service: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves the files and corresponding patches that are included in a change.
   * 
   * @param req The HTTP Request. Should have a param ":number".
   * @param res The HTTP Response.
   * @return A JSON String containing change files and patches.
   * @throws IOException
   */
  public String getChangeFiles(Request req, Response res) {
    String number = req.params(":number");

    String urlString =
        String.format("%s%s%s/%s%s", DIFF_SERVICE_PATH, DIFF_PATH, CHANGES_PATH, number, FILE_PATH);
    logger.entering(CLASS_NAME, "getChangeFiles", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getChangeFiles");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the files for change.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }
}
