

package pygmy.app;

class PathHandler {

    public static String getPingPath() {
        if (checkEnvVar("PING_URL")) {
            return System.getenv("PING_URL");
        }
        return "http://localhost:8001/ping";
    }

    public static String getGithubConnectorPath() {
        if (checkEnvVar("GITHUB_CONNECT_URL")) {
            return System.getenv("GITHUB_CONNECT_URL");
        }
        return "http://localhost:8002";
    }

    public static String getCompilerPath() {
        if (checkEnvVar("COMPILER_URL")) {
            return System.getenv("COMPILER_URL");
        }
        return "http://localhost:8003";
    }

    public static String getParserPath() {
        if (checkEnvVar("PARSER_URL")) {
            return System.getenv("PARSER_URL");
        }
        return "http://localhost:8003";
    }

    public static String getDiffPath() {
        if (checkEnvVar("DIFF_URL")) {
            return System.getenv("DIFF_URL");
        }
        return "http://localhost:8004";
    }

    public static String getAnnotationPath() {
        if (checkEnvVar("ANNOTATION_URL")) {
            return System.getenv("ANNOTATION_URL");
        }
        
        return "http://localhost:8005";
    }

    public static String getStoragePath() {
        if (checkEnvVar("STORAGE_URL")) {
            return System.getenv("STORAGE_URL");
        }
        return "http://localhost:8006";
    }

    public static String getReplayPath() {
        if (checkEnvVar("REPLAY_URL")) {
            return System.getenv("REPLAY_URL");
        } 
        return "http://localhost:8007";
    }

    public static String getFixationPath() {
        if (checkEnvVar("FIXATION_URL")) {
            return System.getenv("FIXATION_URL");
        } 
        return "http://localhost:8008";
    }

    public static String getGerritConnectPath() {
        if (checkEnvVar("GERRIT_CONNECT_URL")) {
            return System.getenv("GERRIT_CONNECT_URL");
        }
        return "http://localhost:8009";
    }

    private static boolean checkEnvVar(String envVar) {
        return System.getenv(envVar) != null;
    }
    
}
