package pygmy.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import spark.Request;
import spark.Response;
//import pygmy.PathHandler;

/** Class for communicating with the Storage Service */
public class FixationService {

  private static final String FIXATION_PATH = PathHandler.getFixationPath();

  private static final String FIXATION_POST_FILE_NMR = "/fixate";
  private static final String FIXATION_REQUEST_STRING = "?path=";
  private static final String CLASS_NAME = FixationService.class.getName();
  private Logger logger;


  public FixationService(Logger logger) {
    this.logger = logger;
    logger.info("Fixation Service started");
  }

  /**
   * Posts a file path to fixation service to fixate. Returns a path to the fixated file.
   * This should be used in replay service to get the fixated file.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The response from the Storage Service as a JSON String.
   * @throws ParseException
   * @throws IOException
   */
    public String postFixationFile(Request req, Response res)  {
        logger.entering(CLASS_NAME, "postFixationFile");
        String urlString = String.format("%s%s%s%s", FIXATION_PATH, FIXATION_POST_FILE_NMR, FIXATION_REQUEST_STRING, req.params("path"));
        System.out.println("postFixationFile in pygmy-service with url: " + urlString);
        try {  
            HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
            if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
              res.status(HttpURLConnection.HTTP_OK);
              String resString = getResponse(c.getInputStream());
              logger.exiting(CLASS_NAME, "postFixationFile", resString);
              System.out.println("Returned: "+resString);
              return resString;
            } else {
              res.status(c.getResponseCode());
              String err = String.format(
                "%d: Could not GET from Fixation Service", c.getResponseCode()); 
              logger.severe(err);
              return err;
            }
        } 
        catch (IOException e) {
            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return String.format("Invalid path: %d", e.getMessage());
        }
    }

  private String getResponse(InputStream res) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(res));
    String inputLine;
    StringBuffer resp = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      resp.append(inputLine);
    }
    return resp.toString();
  }

    
}
