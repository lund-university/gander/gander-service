package pygmy.app;

import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.options;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import spark.Filter;

public class App {
  private static final int DEFAULT_PORT = 8001;
  private static String logsFolderPath;

  enum StatusResponse {
    SUCCESS,
    ERROR
  }
  // TODO: Add status message

  /** Starts the server. */
  public static void main(String[] args) {

    int port = args.length > 0 ? parsePortArgument(args[0]) : DEFAULT_PORT;
    port(port);
    Logger logger = setupLogger(port);
    logger.info("-- STARTING GANDER SERVICE ON PORT " + port + " --");
    PingService s = new PingService(logger);
    PurService pur = new PurService(logger);
    GitService git = new GitService(logger);
    GerritConnectService gerrit = new GerritConnectService(logger);
    CompilerService compiler = new CompilerService(logger);
    ParserService parser = new ParserService(logger);
    DiffService diff = new DiffService(logger);
    AnnotationService annotation = new AnnotationService(logger);
    StorageService storage = new StorageService(logger);
    ReplayService replay = new ReplayService(logger);
    FixationService fixation = new FixationService(logger);


    after(
        (Filter)
            (request, response) -> {
              response.header("Access-Control-Allow-Origin", "*");
              response.header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
              response.header("Access-Control-Allow-Headers", "*");
            });
    options(
        "/*",
        (request, response) -> {
          return "OK";
        });
    get("/ping", (req, res) -> {return "pong";});

    get("/pur/commits/:sha", (req, res)                        -> git.getCommitContent(req, res));
    get("/pur/commits", (req, res)                             -> git.getGitCommits(req, res));
    get("/pur/git/pulls", (req, res)                           -> git.getPullRequestList(req, res));
    get("/pur/git/pulls/:pull_number", (req, res)              -> git.getPullRequest(req, res));
    get("/pur/git/pulls/:pull_number/comments", (req, res)     -> git.getCommentList(req, res));
    get("/pur/git/pulls/:owner/:repo/:pull_number", (req, res) -> git.getSpecificPullRequest(req, res));
    get("/pur/git/:name/:date", (req, res)                     -> git.getContentOfFileUntilDate(req, res));
    get("/pur/git", (req, res)                                 -> git.getFilesInGitRepo(req, res));

    post("/pur", (req, res)                   -> pur.addPur(req, res));
    get("/purs", (req, res)                   -> pur.getPurs(req, res));
    get("/pur/:name", (req, res)              -> pur.getPurFile(req, res));
    put("/pur/:name", (req, res)              -> pur.putPurFile(req, res));
    get("/pur/:name/:nestedFile", (req, res)  -> pur.getPurFile(req, res));
    put("/pur/:name/:nestedFile", (req, res)  -> pur.putPurFile(req, res));

    get("/gerrit/ping", (req, res)    -> gerrit.ping(req, res));
    get("/gerrit/changes", (req, res) -> gerrit.getChangeList(req, res));
    get("/gerrit/changes/:number/comments", (req, res) -> gerrit.getCommentList(req, res));

    get("/diff/pulls/:pull_number", (req, res)                    -> diff.getPullRequestPatch(req, res));
    get("/diff/pulls/:owner/:repo/:pull_number", (req, res)       -> diff.getPullRequestPatchFromUrl(req, res));
    get("/diff/pulls/:pull_number/files", (req, res)              -> diff.getPullRequestFiles(req, res));
    get("/diff/pulls/:owner/:repo/:pull_number/files", (req, res) -> diff.getPullRequestFilesFromUrl(req, res));
    get("/diff", (req, res)                                       -> diff.ping(req, res));
    post("/compareFiles", (req, res)                              -> diff.compareFiles(req, res));
    get("/diff/compare/:commits", (req, res)                      -> diff.getComparisonOfCommits(req, res));
    get("/diff/changes/:number/files", (req, res)                 -> diff.getChangeFiles(req, res));
    
    post("/compiler/use", (req, res) -> compiler.postUse(req, res));
    post("/compiler/ast", (req, res) -> compiler.getASTInformation(req, res));

    get("/parser/ping", (req, res) -> parser.ping(req, res));
    get("/parser/supportedExtensions", (req, res) -> parser.supportedExtensions(req, res));
    post("/parser/parse", (req, res) -> parser.parse(req, res));
    
    get("/annotation/ping", (req, res)              -> annotation.ping(req, res));
    get("/annotation/comments/:mode/:number", (req, res)  -> annotation.getComments(req, res));
    post("/annotation/comments/:number", (req, res) -> annotation.postComment(req, res));

    post("/storeCoordinates", (req, res)    -> storage.storeCoordinates(req, res));
    post("/storeElements", (req, res)       -> storage.storeElements(req, res));
    post("/storeInteraction", (req, res)    -> storage.storeInteraction(req, res));
    post("/initSynchronization", (req, res) -> storage.initSynchronization(req, res));
    post("/createNewFile", (req, res)       -> storage.createNewFile(req, res));
    post("/storeBufferData", (req,res)      -> storage.storeBufferData(req,res));

    post("/path", (req, res)    -> replay.postPath(req, res));
    get("/getpaths", (req, res) -> replay.getPaths(req, res));
    get("/fixatedfile/:file", (req, res)   -> replay.getFixatedFile(req, res));
    //get("/parsefixation/:file", (req, res)   -> replay.getFixatedFile(req, res)); 

    //Posts which file to fixate, returns path to the fixated file
    post("/fixate/:path", (req,res) -> fixation.postFixationFile(req, res));
    //try{
    //Thread.sleep(5000);
    //}
    //catch(Exception e){
    //  System.out.println(e);
    //}  
    //logger.info(annotation.ping(null, null));
    
    
  }

  /**
   * Parses the incoming argument that should be containing the port number.
   *
   * @param portNbr the port number as a String
   * @return the port number as an int
   */
  public static int parsePortArgument(String portNbr) {
    try {
      return Integer.parseInt(portNbr);
    } catch (NumberFormatException e) {
      System.out.println(
          "Could not resolve the argument to a port number, using the default port number "
              + DEFAULT_PORT);
      return DEFAULT_PORT;
    }
  }

  /**
   * Initializes a Logger object. Creates a new entry in the log-folder.
   *
   * @param port port number the service is active on
   * @return the Logger object
   */
  public static Logger setupLogger(int port) {
    createLogFolder();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
    Logger logger = Logger.getLogger(App.class.getName());
    FileHandler fh;
    try {
      fh =
          new FileHandler(
              logsFolderPath + "/Log_" + dtf.format(LocalDateTime.now()) + "_port" + port + ".log",
              true);
      fh.setFormatter(new SimpleFormatter());
      logger.addHandler(fh);
      // Change the line below for more/less logging info
      logger.setLevel(Level.ALL);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return logger;
  }
  
  /** Creates a folder for the logs, if it doesn't already exist. */
  private static void createLogFolder() {
    logsFolderPath = Paths.get("logs").toAbsolutePath().toString();
    try {
      Files.createDirectories(Paths.get(logsFolderPath));
    } catch (IOException e) {
      System.out.println("Error while creating the logs directory. ");
    }
  }
}
