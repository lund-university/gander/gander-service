package pygmy.app;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.logging.Logger;
import spark.Request;
import spark.Response;
//import pygmy.PathHandler;

public class ParserService {


  private static final String PARSE_FILE_PATH = "/parse";
  private static final String GET_EXTENSIONS_PATH = "/supportedExtensions";
  private static final String PING_PATH = "/ping";
   
  private static final String PARSER_PATH = PathHandler.getParserPath();

  private static final String CLASS_NAME = ParserService.class.getName();
  private Logger logger;

  public ParserService(Logger log) {
    this.logger = log;
    logger.info("parserService started");
  }

  public String parse(Request req, Response res) {

    String urlString = String.format("%s%s", PARSER_PATH, PARSE_FILE_PATH);
    String b = req.body();
    logger.entering(CLASS_NAME, "parse", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);
      logger.fine("1");
      try (OutputStream os = c.getOutputStream()) {
        byte[] input = b.getBytes("utf-8");
        os.write(input, 0, input.length);
      }
      logger.fine("2");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        res.status(HttpURLConnection.HTTP_OK);
        logger.exiting(CLASS_NAME, "parse", c.getInputStream().toString());
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String errUseDef =
            String.format("%d: Error while retrieving the parsed file.", c.getResponseCode());
        logger.severe(errUseDef);
        logger.severe(c.getResponseMessage());
        return errUseDef;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  public String supportedExtensions(Request req, Response res) {
    String urlString = String.format("%s%s", PARSER_PATH, GET_EXTENSIONS_PATH);
    logger.entering(CLASS_NAME, "supportedExtensions", urlString);

    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");
      
      logger.fine("OK");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        res.status(HttpURLConnection.HTTP_OK);
        String response = JSONUtils.processArrayResponse(c.getInputStream());
        logger.exiting(CLASS_NAME, "supportedExtensions", response);
        return response;
      } else {
        res.status(c.getResponseCode());
        String errUseDef =
            String.format("%d: Error while retrieving supported extensions.", c.getResponseCode());
        logger.severe(errUseDef);
        logger.severe(c.getResponseMessage());
        return errUseDef;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    } 
  }

  public String ping(Request req, Response res) {
    String urlString = String.format("%s%s", PARSER_PATH, PING_PATH);
    logger.entering(CLASS_NAME, "ping", urlString);

    try {
        HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");

        logger.fine("OK");
        if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
            logger.exiting(CLASS_NAME, "ping");
            String response = JSONUtils.processArrayResponse(c.getInputStream());
            logger.fine("response: " + response);
            res.status(HttpURLConnection.HTTP_OK);
            return response;
        } else {
            res.status(c.getResponseCode());
            logger.severe(String.format("%d: Error while retrieving the ping from url: %s", c.getResponseCode(), urlString));
            return String.format("%d: Error while retrieving the ping.", c.getResponseCode());
        }
    } catch (IOException e) {
        res.status(HttpURLConnection.HTTP_BAD_REQUEST);
        logger.severe(String.format("Invalid path: %s", e.getMessage()));
        return String.format("Invalid path: %s", e.getMessage());
    }
}
}
