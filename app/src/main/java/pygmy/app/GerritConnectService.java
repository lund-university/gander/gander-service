package pygmy.app;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import spark.Request;
import spark.Response;

public class GerritConnectService {
  private static final String GERRIT_CONNECT_SERVICE_PATH = PathHandler.getGerritConnectPath();
  private static final String GERRIT_PING = "/ping";
  private static final String GERRIT_PATH = "/gerrit";
  private static final String CHANGES_PATH = "/changes";
  private static final String COMMENTS_PATH = "/comments";
  private static final String CLASS_NAME = GerritConnectService.class.getName();

  private Logger logger;

  public GerritConnectService(Logger log) {
    this.logger = log;
    logger.info("GerritConnectService started");
  }

  /**
   * Method for testing the communication with the Gerrit Connect Service.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the response.
   * @throws IOException
   */
  public String ping(Request req, Response res) {
    logger.entering(CLASS_NAME, "ping");
    String urlString = String.format("%s%s", GERRIT_CONNECT_SERVICE_PATH, GERRIT_PING);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "ping");
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String errPing = String.format("%d: Error while pinging the gerrit connect service.", c.getResponseCode());
        logger.severe(errPing);
        return errPing;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path to gerrit connect service: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves the list of all changes from the server.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the list all changes.
   * @throws IOException
   */
  public String getChangeList(Request req, Response res) {
    String urlString = String.format("%s%s%s", GERRIT_CONNECT_SERVICE_PATH,
        GERRIT_PATH, CHANGES_PATH);
    logger.entering(CLASS_NAME, "getChangeList", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getChangeList");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err = String.format("%d: Error while retrieving changes.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  public Object getCommentList(Request req, Response res) {
    String number = req.params(":number");
    String urlString = String.format("%s%s%s/%s%s", GERRIT_CONNECT_SERVICE_PATH,
        GERRIT_PATH, CHANGES_PATH, number, COMMENTS_PATH);
    logger.entering(CLASS_NAME, "getCommentList", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getCommentList");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err = String.format("%d: Error while retrieving comments.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }
}
