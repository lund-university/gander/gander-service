package pygmy.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;
import spark.Request;
import spark.Response;
//import pygmy.PathHandler;

/** Class for communicating with the Pur Service for the Git actions. */
class GitService {

  private static final String GIT_SERVICE_PATH = PathHandler.getGithubConnectorPath();

  private static final String COMMIT_PATH = "/pur/commits";
  private static final String GIT_PATH = "/git";
  private static final String PULL_PATH = "/pulls";
  private static final String COMMENTS_PATH = "/comments";
  private static final String CLASS_NAME = GitService.class.getName();
  private Logger logger;

  public GitService(Logger log) {
    this.logger = log;
    logger.info("GitService started");
  }

  /**
   * Retrieves the commit content for the specified sha-code.
   * 
   * @param req The HTTP Request. Should contain the param ":sha"
   * @param res The HTTP Response.
   * @return A JSON String containing the commit content.
   * @throws IOException
   */
  public String getCommitContent(Request req, Response res) {
    String sha = req.params(":sha");

    String urlString = String.format("%s%s%s", GIT_SERVICE_PATH, COMMIT_PATH, sha);
    logger.entering(CLASS_NAME, "getCommitContent", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getCommitContent");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err = String.format("%d: Error while retrieving the commit.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves the list of all of the pull requests from the repo.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the list of all the pull requests.
   * @throws IOException
   */
  public String getPullRequestList(Request req, Response res) {
    String urlString = String.format("%s%s%s", GIT_SERVICE_PATH, GIT_PATH, PULL_PATH);
    logger.entering(CLASS_NAME, "getPullRequestList", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPullRequestList");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format("%d: Error while retrieving pull requests.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves a pull request with the specified pull request number.
   * 
   * @param req The HTTP Request. Should contain the param ":pull_number"
   * @param res The HTTP Response.
   * @return A JSON String containing the pull request with its content.
   * @throws IOException
   */
  public String getPullRequest(Request req, Response res) {
    logger.entering(CLASS_NAME, "getPullRequest");
    String pullNumber = req.params(":pull_number");

    String urlString =
        String.format("%s%s%s/%s", GIT_SERVICE_PATH, GIT_PATH, PULL_PATH, pullNumber);
    logger.entering(CLASS_NAME, "getPullRequest", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getPullRequest");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the content of a pull request from source url: %s" , c.getResponseCode(), urlString);
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  public String getSpecificPullRequest(Request req, Response res) {
    logger.entering(CLASS_NAME, "getSpecificPullRequest");
    String owner = req.params(":owner");
    String repo = req.params(":repo");
    String pullNumber = req.params(":pull_number");
       
    String urlString =
     String.format("%s%s%s/%s/%s/%s", GIT_SERVICE_PATH, GIT_PATH, PULL_PATH, owner, repo, pullNumber);
            
    logger.entering(CLASS_NAME, "getSpecificPullRequest", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getSpecificPullRequest");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the content of a pull request from source url: %s" , c.getResponseCode(), urlString);
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves an older version of a file, if it is not currently shown.
   * @param req The HTTP Request. Should contain the params ":date" and ":name".
   * @param res The HTTP Response.
   * @return A JSON String containing the content of a file.
   * @throws IOException
   */
  public String getContentOfFileUntilDate(Request req, Response res)
      throws UnsupportedEncodingException {
    String date = req.params(":date");
    String name = URLEncoder.encode(req.params(":name"), StandardCharsets.UTF_8.toString());

    String urlString = String.format("%s%s/%s/%s", GIT_SERVICE_PATH, GIT_PATH, name, date);
    logger.entering(CLASS_NAME, "getContentOfFileUntilDate", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getContentOfFileUntilDate");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while retrieving the content of the file until date.",
                c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves all of the files that is currently in the git repo on the master branch.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return A JSON String containing the list of existing files.
   * @throws IOException
   */
  public String getFilesInGitRepo(Request req, Response res) {

    String urlString = String.format("%s%s", GIT_SERVICE_PATH, GIT_PATH);
    logger.entering(CLASS_NAME, "getFilesInGitRepo", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getFilesInGitRepo");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format("%d: Error while retrieving the git repo content.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves the list of all of the commits that has been done to the master branch.
   * 
   * @param req The HTTP Request. Should contain the param ":name".
   * @param res The HTTP Response.
   * @return A JSON String containing the list of git commits.
   * @throws IOException
   */
  public String getGitCommits(Request req, Response res) {
    String name = req.queryParams("name");
    String urlString;
    if (name != null) {
      urlString = String.format("%s%s?name=%s", GIT_SERVICE_PATH, COMMIT_PATH, name);

    } else {
      urlString = String.format("%s%s", GIT_SERVICE_PATH, COMMIT_PATH);
    }
    logger.entering(CLASS_NAME, "getGitCommits", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getGitCommits");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format("%d: Error while retrieving the git commits.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Retrieves a list of all the comments that has been done in the pull request.
   * 
   * @param req The HTTP Request. <p> Should contain the param ":pull_number".
   * @param res The HTTP Response.
   * @return A JSON String containing the list of comments.
   * @throws IOException
   */
  public String getCommentList(Request req, Response res) {
    String pullNumber = req.params(":pull_number");
    String urlString =
        String.format(
            "%s%s%s/%s%s", GIT_SERVICE_PATH, GIT_PATH, PULL_PATH, pullNumber, COMMENTS_PATH);
    logger.entering(CLASS_NAME, "getCommentList", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getCommentList");
        return JSONUtils.processExternalResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format("%d: Error while retrieving pull requests.", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }
}
