package pygmy.app;


import java.util.logging.Logger;

import spark.Request;
import spark.Response;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ResponseCache;
//import pygmy.PathHandler;

/** Class for communicating with ReplayService */
public class ReplayService {
    private Logger logger;

    private static final String PATH_ADDRESS = PathHandler.getReplayPath();

    private static final String CLASS_NAME = ReplayService.class.getName();
    private static final String PATH_PATH = "/path";
    private static final String PATH_GET_PATHS="/getpaths";
    private static final String GET_FIXATED_FILE = "/fixatedfile/";

    public ReplayService(Logger log ) {
        this.logger = log;
        logger.info("ReplayService started");
    }

    public String getPaths(Request req, Response res){
        String urlString =  String.format("%s%s", PATH_ADDRESS, PATH_GET_PATHS);
        logger.entering(CLASS_NAME, "getPaths", urlString);
        

        try {
            HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");
           
            if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
                logger.exiting(CLASS_NAME, "ping");
                return JSONUtils.processResponse(c.getInputStream());
              } else {
                res.status(c.getResponseCode());
                String err =
                    String.format("%d: Error while getting paths", c.getResponseCode());
                logger.severe(err);
                return err;
              }
            } catch (IOException e) {
              res.status(HttpURLConnection.HTTP_BAD_REQUEST);
              String errExc = String.format("Invalid path: %d", e.getMessage());
              logger.severe(errExc);
              return errExc;
            }
    }

    /**
     * 
     * @param req The HTTP request
     * @param res the HTTP response
     * @return The response from the Replay Service as a JSON String
     * @throws IOException
     */

    public String postPath(Request req,  Response res) {

        String urlString =  String.format("%s%s", PATH_ADDRESS, PATH_PATH);
        String pathData = req.body();
        logger.entering(CLASS_NAME, "path", urlString);
        logger.finer(pathData);

        try {
            HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
            c.setDoOutput(true);
            try (OutputStream os = c.getOutputStream()) {
                byte[] input = pathData.getBytes("utf-8");
                os.write(input, 0, input.length );
            }
            if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
                logger.exiting(CLASS_NAME,"path");
                return JSONUtils.processArrayResponse(c.getInputStream());
            } else {
                res.status(c.getResponseCode());
                String err = String.format("%d: Could not send path to Replay Service", c.getResponseCode());
                logger.severe(err);
                return err;
            }
        } catch (IOException e) {
            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            String errExc = String.format("Invalid path: %d", e.getMessage());
            return errExc;
        }
    }

    /** Get the fixated version of choosen file.
     * If not already fixated, replay-service will fixate and parse the file.
     * 
     * @param req
     * @param res
     * @return
     */
    public String getFixatedFile(Request req, Response res) {
        logger.entering(CLASS_NAME, "getFixatedFile");

        String fileName = req.params("file");
        String urlString = String.format("%s%s%s", PATH_ADDRESS, GET_FIXATED_FILE, fileName);

        logger.finer(fileName);

        try {
            HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "GET");

             res.status(HttpURLConnection.HTTP_OK);

            if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return JSONUtils.processResponse(c.getInputStream());
            } else {
                res.status(c.getResponseCode());
                logger.severe("Error " + c.getResponseCode());
                return "Error " + c.getResponseMessage();
            }
        } catch (Exception e ) {
            logger.severe(e.getMessage());
            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Exception " + e.getMessage();
        }
    }

    
}
