package pygmy.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import spark.Request;
import spark.Response;
//import pygmy.PathHandler;

/**
 * Used for testing the connection to the Pygmy Service.
 *  
 * <p>TODOS: - Cleanup: Implement better error handling */
class PingService {
  private static final String URL_STRING = PathHandler.getPingPath();
  private static final String CLASS_NAME = PurService.class.getName();
  private Logger logger;

  public PingService(Logger log) {
    this.logger = log;
    logger.info("PingService started");
  }

  /**
   * Ping method to check the connection.
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The status message as a JSON String.
   * @throws IOException
   */
  public String ping(Request req, Response res) throws IOException {

    logger.entering(CLASS_NAME, "ping");
    JSONObject obj = new JSONObject();
    obj.put("message", "pong");
    String jsonText = obj.toString();
    res.body(jsonText);
    res.status(200);
    logger.exiting(CLASS_NAME, "ping", jsonText);
    return jsonText;
  }

}
