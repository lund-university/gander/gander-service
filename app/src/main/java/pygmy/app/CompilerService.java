package pygmy.app;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.logging.Logger;
import spark.Request;
import spark.Response;
//import pygmy.PathHandler;

public class CompilerService {


  private static final String USE_PATH = "/use";
  private static final String AST_PATH = "/ast";
   
  private static final String COMPILE_PATH = PathHandler.getCompilerPath();

  private static final String CLASS_NAME = CompilerService.class.getName();
  private Logger logger;

  public CompilerService(Logger log) {
    this.logger = log;
    logger.info("compilerService started");
  }

  public String postUse(Request req, Response res) {

    String urlString = String.format("%s%s", COMPILE_PATH, USE_PATH);
    String b = req.body();
    logger.entering(CLASS_NAME, "postUse", urlString);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);
      logger.fine("1");
      try (OutputStream os = c.getOutputStream()) {
        byte[] input = b.getBytes("utf-8");
        os.write(input, 0, input.length);
      }
      logger.fine("2");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        res.status(HttpURLConnection.HTTP_OK);
        logger.exiting(CLASS_NAME, "postUse", c.getInputStream().toString());
        return JSONUtils.processResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String errUseDef =
            String.format("%d: Error while retrieving the use def.", c.getResponseCode());
        logger.severe(errUseDef);
        logger.severe(c.getResponseMessage());
        return errUseDef;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  public String getASTInformation(Request req, Response res) {
    
    String urlString = String.format("%s%s", COMPILE_PATH, AST_PATH);
    logger.entering(CLASS_NAME, "getASTInformation", urlString);
    
    String b = req.body();
    logger.fine(b);
    
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {
        byte[] input = b.getBytes("utf-8");
        os.write(input, 0, input.length);
      }
      logger.fine("OK");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.exiting(CLASS_NAME, "getASTInformation");
        String response = JSONUtils.processArrayResponse(c.getInputStream());
        logger.fine("response: "+  response);
        res.status(HttpURLConnection.HTTP_OK);
        return response;
      } else {
        res.status(c.getResponseCode());
        logger.severe(
            String.format("%d: Error while retrieving the ast information from url: %s", c.getResponseCode(), urlString));
        return String.format(
            "%d: Error while retrieving the ast information.", c.getResponseCode());
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      logger.severe(String.format("Invalid path: %s", e.getMessage()));
      return String.format("Invalid path: %s", e.getMessage());
    }
  }
}
