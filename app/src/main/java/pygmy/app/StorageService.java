package pygmy.app;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.logging.Logger;
import spark.Request;
import spark.Response;
////import pygmy.PathHandler;
/** Class for communicating with the Storage Service */
class StorageService {
  
  private static final String STORAGE_ADDRESS = PathHandler.getStoragePath();

  private static final String STORAGE_STORE_COORDS = "/storeCoordinates";
  private static final String STORAGE_STORE_BUFFER_DATA = "/storeBufferData";
  private static final String STORAGE_STORE_ELEMENTS = "/storeElements";
  private static final String STORAGE_STORE_INTERACTION = "/storeInteraction";
  private static final String STORAGE_SYNCH_TIMESTAMPS = "/initSynchronization";
  private static final String STORAGE_CREATE_FILE = "/createNewFile";
  private static final String CLASS_NAME = StorageService.class.getName();
  private Logger logger;

  public StorageService(Logger log) {
    this.logger = log;
    logger.info("StorageService started");
  }

  /**
   * Sends the coordinate for storage to the Storage Service.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The response from the Storage Service as a JSON String.
   * @throws IOException
   */
  public String storeCoordinates(Request req, Response res) {
    String urlString = String.format("%s%s", STORAGE_ADDRESS, STORAGE_STORE_COORDS);
    String coordinateData = req.body();
    logger.entering(CLASS_NAME, "storeCoordinates", urlString);
    logger.finer(coordinateData);
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {

        byte[] input = coordinateData.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
        logger.exiting(CLASS_NAME, "storeCoordinates");
        return JSONUtils.processArrayResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Could not send eye-tracking data to Storage Service", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }
  /**
   * Send buffer data to storage, to be stored as buffer data logs
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The response from the Storage Service as a JSON String.
   * @throws IOException
   */
  public String storeBufferData(Request req, Response res) {
    String urlString = String.format("%s%s", STORAGE_ADDRESS, STORAGE_STORE_BUFFER_DATA);
    String bufferData = req.body();
    logger.entering(CLASS_NAME, "storeBufferData", urlString);
    logger.finer(bufferData);


    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {

        byte[] input = bufferData.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
        logger.exiting(CLASS_NAME, "storeBufferData");
        return JSONUtils.processArrayResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Could not send buffer data to Storage Service", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Sends the elements for storage to the Storage Service.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The response from the Storage Service as a JSON String.
   * @throws IOException
   */
  public String storeElements(Request req, Response res) {
    String urlString =
        String.format("%s%s", STORAGE_ADDRESS, STORAGE_STORE_ELEMENTS);
    String elementData = req.body();
    logger.entering(CLASS_NAME, "storeElements", urlString);
    logger.finer(elementData);

    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {

        byte[] input = elementData.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
        logger.exiting(CLASS_NAME, "storeElements");
        return JSONUtils.processArrayResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Could not send the tracked elements to Storage Service", c.getResponseCode());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Sends the interaction events for storage to the Storage Service.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The response from the Storage Service as a JSON String.
   * @throws IOException
   */
  public String storeInteraction(Request req, Response res) {
    String urlString =
        String.format("%s%s", STORAGE_ADDRESS, STORAGE_STORE_INTERACTION);
    String interactionData = req.body();
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {

        byte[] input = interactionData.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
        return JSONUtils.processArrayResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        return String.format(
            "%d: Could not send the tracked interaction to Storage Service", c.getResponseCode());
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      return String.format("Invalid path: %d", e.getMessage());
    }
  }

  /**
   * Sends the commando to the Storage Service to initialize timestamp synchronization.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The response from the Storage Service as a JSON String.
   */
  public String initSynchronization(Request req, Response res) {
    String urlString =
        String.format("%s%s", STORAGE_ADDRESS, STORAGE_SYNCH_TIMESTAMPS);
    String synchData = req.body();
    try {
      HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
      c.setDoOutput(true);

      try (OutputStream os = c.getOutputStream()) {

        byte[] input = synchData.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (c.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
        return JSONUtils.processArrayResponse(c.getInputStream());
      } else {
        res.status(c.getResponseCode());
        return String.format(
            "%d: Could not send the synch data to Storage Service", c.getResponseCode());
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      return String.format("Invalid path: %d", e.getMessage());
    }
  }

  /**
   * Sends the commando to the Storage Service to create a new action log file.
   * 
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The response from the Storage Service as a JSON String.
   */
  public String createNewFile(Request req, Response res) {
    String urlString =
    String.format("%s%s", STORAGE_ADDRESS, STORAGE_CREATE_FILE);
    String Data = req.body();
    try {
    HttpURLConnection c = HTTPConnectionUtils.setConnection(urlString, "POST");
    c.setDoOutput(true);

    try (OutputStream os = c.getOutputStream()) {

      byte[] input = Data.getBytes("utf-8");
      os.write(input, 0, input.length);
    }

    if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
      return JSONUtils.processResponse(c.getInputStream());
    } else {
      res.status(c.getResponseCode());
      return String.format(
          "%d: Could not create new file in Storage Service", c.getResponseCode());
    }
    } catch (IOException e) {
    res.status(HttpURLConnection.HTTP_BAD_REQUEST);
    return String.format("Invalid path: %d", e.getMessage());
    }
  }
}
