package pygmy.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/** A utils class for handling JSON Objects */
class JSONUtils {

  /**
   * Parses the response as a JSON Object
   *
   * @param res InputStream response from a request.
   * @return JSONObject as a string.
   */
  public static String processResponse(InputStream res) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(res));
    String inputLine;
    StringBuffer resp = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      resp.append(inputLine);
    }
    in.close();
    try {
      JSONParser jsonParser = new JSONParser();
      JSONObject jsonObject = (JSONObject) jsonParser.parse(resp.toString());
      return jsonObject.toString();

    } catch (Exception e) {
      return String.format("JSON Parser exception: %d", e.getMessage());
    }
  }

  /**
   * Parses the response as a string.
   *
   * @param res InputStream response from a request.
   * @return The string read from the input stream.
   */
  public static String processExternalResponse(InputStream res) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(res));
    String inputLine;
    StringBuffer resp = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      resp.append(inputLine);
    }
    in.close();
    return resp.toString();
  }

  /**
   * Parses the response as a JSON Object
   *
   * @param res InputStream response from a request.
   * @return JSONArray
   */
  public static String processArrayResponse(InputStream res) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(res));
    String inputLine;
    StringBuffer resp = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      resp.append(inputLine);
    }
    in.close();
    return resp.toString();
  }
}
