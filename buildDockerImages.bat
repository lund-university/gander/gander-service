@ECHO OFF

IF "%1"=="" GOTO portsNotIncluded

set ganderPort=%1
set /a guthubConnectPort=%1+1
set /a compilerPort=%1+2
set /a diffPort=%1+3
set /a annotationPort=%1+4
set /a storagePort=%1+5
set /a gerritConnectPort=%1+8
GOTO startingServices

:portsNotIncluded
set ganderPort       =   8001
set guthubConnectPort         =   8002
set compilerPort    =   8003
set diffPort        =   8004
set annotationPort  =   8005
set storagePort     =   8006
set replayPort      =   8007
set fixationPort    =   8008
set gerritConnectPort         =   8009
ECHO Ports not defined, using default port numbers ..

%docker build -t name_of_service:latest
:startingServices

ECHO building docker image of Gander Service.. 
start cmd.exe /k "TITLE Gander Service & gradle docker"

ECHO Gander service image created!

ECHO building docker image of Gander Client..
cd ../gander-client
start cmd.exe /k "TITLE Gander Client & docker build -t gander-client:latest ."

ECHO Gander client image created!!

ECHO building docker image of githubConnect Service..
cd ../github-connect-service
start cmd.exe /k "TITLE githubConnect Service & gradle docker"
ECHO githubConnect service image created!!

@REM ECHO building docker image of Compiler Service..
@REM cd ../compiler-service
@REM start cmd.exe /k "TITLE Compiler Service & docker build -t compiler-service:latest ."
@REM ECHO Compiler service image created!!

ECHO building docker image of Parser Service..
cd ../parser-service
start cmd.exe /k "TITLE Parser Service & gradle docker"
ECHO Parser service image created!!

ECHO building docker image of Diff Service..
cd ../diff-service
start cmd.exe /k "TITLE Diff Service & gradle docker"
ECHO Diff service image created!!

ECHO building docker image of Annotation Service..
cd ../annotation-service
start cmd.exe /k "TITLE Annotation Service & gradle docker"
ECHO Annotation service image created!!

ECHO building docker image of Storage Service..
cd ../storage-service
start cmd.exe /k "TITLE Storage Service & gradle docker"
ECHO Storage service image created!!

ECHO building docker image of Replay Service..
cd ../replay-service
start cmd.exe /k "TITLE Replay Service & gradle docker"
ECHO Replay service image created!!

ECHO building docker image of Gerrit Connect Service..
cd ../gerrit-connect-service
start cmd.exe /k "TITLE Gerrit Connect Service & gradle docker"
ECHO Gerrit Connect service image created!!

cd ../../gander-service
PAUSE