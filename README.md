# Gander Service.
The gander prototype is a web application with support for code review studies with eye tracking functionalities like support for scrolling, annotation of eye tracking coordinates as well as many others, you can find more information about the prototype [here](https://portal.research.lu.se/sv/publications/gander-a-platform-for-exploration-of-gaze-driven-assistance-in-co).

Most of the services in the prototype can be run in docker, except the fixation service and your choice of eye tracking software. 

## Requirements

- Docker (>27.3.1) (only if running in docker)
- Java Development Kit (>15.0.1)

- Gradle (>7.4)

- Python (>3.10.11)

- Eyetracking software of choice, e.g. Tobii

## How to start the GANDER prototype with docker
Clone this repository and all the other in this project. See the GitHub Authentication step below before compiling and running the prototype. 

For an automated process of compiling and creating all docker images run the `buildDockerImages.bat` script.

To do it manually, cd into the respective service (except `compiler-service` and `gander-client`), and run `gradle docker`.

For `compiler-service` and `gander-client` run:
`docker build -t "name_of_service":latest .`

To run the prototype cd into `gander-service/app` and run:
`docker compose up`

Then you should open another terminal and to start `fixation-service` you run:
`python3 App/main.py`

Then you should also make sure your eye-tracking software is up and running and is connected to `fixation-service`

## How to start the GANDER prototype without docker
You should cd into `gander-service` and for windows run:
`startServices.bat`

For Linux and Mac you should run the `startServices.sh` script instead. 

## GitHub Authentication
The services: `annotaion service`, `diff service`, and `github-connect service` all require an extra setup step to allow the GANDER prototype to work properly with GitHub, namely a `credentials.txt` file. See respective READMEs of the mentioned services for a step-by-step guide.

## Eye tracking guide
The eye tracker we used is a Tobii 4c and to get access to its data points in real time we used the [Titta Websocket](https://github.com/dcnieho/Titta/tree/master) to get a stream of coordinates. You can of course use what hardware is available and thus might need to change in `fixation-service` and its corresponding `websocket client` or add your own way of sending the information. More information about the format of eye tracking information can be seen in `fixation-service` README. 

## Environment Variables
Some variables are used in several services and need to be simple to configure before setting up a demo or experiment. These variables are set as Environment Variables in the Docker Compose file, located at GANDER-SERVICE/app/docker-compose.yml. As of now the fields defined as environment variables are the local addresses of other services connected to gander service as well as the url to the target GitHub repo supplied to github-connect-service and diff-service.

## Testing commandos of the service

How to test the server in the terminal:

To add a project to review:  `curl -d '{"name":"gander"}' -X POST http://localhost:8001/pur`

To get all added projects: `curl -X GET localhost:8001/purs`

To get all commits: `curl -X GET localhost:8001/pur/commits`

To get all commits to a specific file: `curl -X GET 'localhost:8001/pur/commits?name={pathToFile}'`

To get the content of a commits: `curl -X GET localhost:8001/pur/commits/{commitSha}`

To get the content of a file: `curl -X GET localhost:8001/pur/{pathToFile}`

To get the content of a file from a specific commit: `curl -X GET 'localhost:8001/pur/{pathToFile}?sha={commitSha}'`

To get the files from the git repository: `curl -X GET localhost:8001/pur/git`


To get the files from the git repository: `curl -X GET localhost:8001/diff/compare/sha1...sha2`

To compare two files: `curl -d '{"src":"content of src", "target":"content of target"}' -X POST localhost:8001/compareFiles`

To get the content of a file until a date: `curl -X GET localhost:8001/pur/git/{pathToFile}/{date}`

To format the code and start the server with the same gradle command: `./gradlew cleanrun`

To generate use def: `curl -d "file content" -X  POST localhost:8001/compiler/use`

To get all pull requests (open and closed):  `curl -X GET localhost:8001/pur/git/pulls`

To get a specific pull request:  `curl -X GET localhost:8001/pur/git/pulls/{pull_number}`

To get the patch for a pull request: `curl -X GET localhost:8001/diff/pulls//{pull_number}`

To post an tracked element to the storage service: `curl -d '[{ "ts": "000", "element" : "button", "element-id" : "submit-button", "coordinates" : {"x": "0", "y" : "12"}}, {"ts": "001", "element" : "div", "element-id" : "content-area", "coordinates" : {"x": "400", "y" : "100"}}]' -X POST localhost:8001/storeElements`




