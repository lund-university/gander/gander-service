#!/bin/bash

# Check port argument
if [ -z "$1" ]; then
  PORT=8001
else
  PORT=$1
fi
echo "Using port: $PORT"

# Check directory argument
if [ -z "$2" ]; then
  DIR="~/pygmy"
else
  DIR=$2
fi
echo "Using directory: $DIR"

#
# Starts a service.
#
# Arg 1: The name of the service (without "service").
# Arg 2: The path to the service directory.
# Arg 3: The port to start the service on.
#
start_service() {
  echo -n "Starting $1 Service at port $3 ... "
  if [[ $OSTYPE == 'darwin'* ]]; then
    osascript -e 'tell app "Terminal"
        do script "cd  '$2' && ./gradlew run --args='$3'"
    end tell'
  else # let's assume Linux and tmux
      tmux new -s $1 -d 
      tmux send -t $1.0 "cd $2" ENTER
      tmux send -t $1.0 "./gradlew run --args=$3" ENTER
  fi
  echo "done."
}

start_service "Pygmy" "$DIR/pygmy-service" $PORT

let "PORT++"

start_service "Pur" "$DIR/pur-service" $PORT

let "PORT++"

start_service "Compiler" "$DIR/compiler-service" $PORT

let "PORT++"

start_service "Diff" "$DIR/diff-service" $PORT

let "PORT++"

start_service "Annotation" "$DIR/annotation-service" $PORT

let "PORT++"

start_service "Storage" "$DIR/storage-service" $PORT

exit 0
